<?php
session_start();
set_time_limit(300);

if (isset($_POST['acao']) && $_POST['acao'] == 'upload') {
    upload();
}

if (!empty($_POST)) {
    $arrFiltros = isset($_POST['arrFiltros']) ? $_POST['arrFiltros'] : array();
} elseif (isset($_SESSION['auditor_filtros'])) {
    $arrFiltros = $_SESSION['auditor_filtros'];
} else {
    $arrFiltros = array();
}
$_SESSION['auditor_filtros'] = $arrFiltros;

if (isset($_SESSION['auditor_filename'])) {
    $arrLog = processa($_SESSION['auditor_filename'], $arrFiltros);
} else {
    $arrLog = array();
}
if (isset($_SESSION['auditor_uploaderr'])) {
    $strErrorMsg = $_SESSION['auditor_uploaderr'];
    unset($_SESSION['auditor_uploaderr']);
} else {
    $strErrorMsg = '';
}

function upload()
{
    $error = isset($_FILES["arquivo"]["error"]) ? $_FILES["arquivo"]["error"] : null;
    if ($error == UPLOAD_ERR_OK) {
        $strTmpFileName = $_FILES['arquivo']['tmp_name'];
        $strFileName = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid() . '.log';

        move_uploaded_file($strTmpFileName, $strFileName);

        $_SESSION['auditor_filename'] = $strFileName;
        $_SESSION['auditor_uploaderr'] = null;
    } else {
        $_SESSION['auditor_filename'] = null;
        $_SESSION['auditor_uploaderr'] = 'Erro ao fazer upload do Arquivo';
    }
    echo "<script>window.location='" . $_SERVER['PHP_SELF'] . "'</script>"; //evita F5
    die;
}

function processa($strFileName, $arrFiltros)
{
    $arrLog = array();

    if (file_exists($strFileName) && is_readable($strFileName)) {
        $fpArquivo = fopen($strFileName, 'r');
        $strBloco = null;
        while (!feof($fpArquivo)) {
            $strLinha = fgets($fpArquivo);

            if (preg_match('/--([a-z0-9]+)-([A-Z])--/', $strLinha, $arrMatches)) {
                $strIdent = $arrMatches[1];
                $strBloco = $arrMatches[2];
                $arrRegistro = &$arrLog[$strIdent];
                if (empty($arrLog[$strIdent])) {
                    $arrRegistro = array(
                        'ident'      => $strIdent,
                        'data_orig'  => '',
                        'data'       => '',
                        'host'       => '',
                        'ip_remoto'  => '',
                        'metodo'     => '',
                        'uri'        => '',
                        'protocolo'  => '',
                        'origin'     => '',
                        'user-agent' => '',
                        'referer'    => '',
                        'cookie'     => '',
                        'post'       => '',
                        'error'      => '',
                        'file'       => '',
                        'id'         => '',
                        'mensagem'   => '',
                        'dados'      => '',
                        'args'       => '');
                }
            }

            switch ($strBloco) {
                case 'A':
                    if (preg_match('/\[([a-zA-Z0-9:\/]+).*\]/', $strLinha, $arrMatches)) {
                        $arrRegistro['data_orig'] = $arrMatches[1];

                        $objDate = date_create_from_format('d/M/Y:H:i:s', $arrMatches[1], new DateTimeZone(date_default_timezone_get()));
                        if ($objDate) {
                            $arrRegistro['data'] = $objDate->format('Y-m-d H:i:s');
                        }
                    }
                    if (preg_match('/(([0-9]{1,3}\.){3}[0-9]{1,3})/', $strLinha, $arrMatches)) {
                        $arrRegistro['ip_remoto'] = $arrMatches[1];
                    }
                    break;
                case 'B':
                    if (preg_match('/^Host:(.*)$/', $strLinha, $arrMatches)) {
                        $arrRegistro['host'] = trim($arrMatches[1]);
                    }
                    if (preg_match('/^X-Forwarded-For:(.*)$/', $strLinha, $arrMatches)) {
                        $arrRegistro['ip_remoto'] = trim($arrMatches[1]);
                    }
                    if (preg_match('/^(POST|GET|PUT|PROPFIND|DELETE|TRACE|CONNECT|HEAD|OPTIONS) (.*) (HTT.*)$/', $strLinha, $arrMatches)) {
                        $arrRegistro['metodo']    = trim($arrMatches[1]);
                        $arrRegistro['uri']       = htmlentities(urldecode($arrMatches[2]));
                        $arrRegistro['protocolo'] = trim($arrMatches[3]);
                    }
                    if (preg_match('/^Origin:(.*)$/', $strLinha, $arrMatches)) {
                        $arrRegistro['origin'] = trim($arrMatches[1]);
                    }
                    if (preg_match('/^User-Agent:(.*)$/', $strLinha, $arrMatches)) {
                        $arrRegistro['user_agent'] = trim($arrMatches[1]);
                    }
                    if (preg_match('/^Referer:(.*)$/', $strLinha, $arrMatches)) {
                        $arrRegistro['referer'] = trim($arrMatches[1]);
                    }
                    if (preg_match('/^Cookie:(.*)$/', $strLinha, $arrMatches)) {
                        $arrRegistro['cookie'] = trim($arrMatches[1]);
                    }
                    break;
                case 'C':
                    if (!preg_match('/-C--/', $strLinha)) {
                        if (isset($arrRegistro['post']) && !empty($arrRegistro['post'])) {
                            $arrRegistro['post'] .= "\n" . urldecode(trim($strLinha));
                        } else {
                            $arrRegistro['post'] = urldecode(trim($strLinha));
                        }
                    }
                    break;
                case 'F':
                    if (preg_match('/^HTTP[S]?[\/[0-9\.]*(.*)/', $strLinha, $arrMatches)) {
                        $arrRegistro['error'] = trim($arrMatches[1]);
                    }
                    break;
                case 'H':
                    if (!preg_match('/-H--/', $strLinha)) {
                        if (isset($arrRegistro['mensagem']) && !empty($arrRegistro['mensagem'])) {
                            $arrRegistro['mensagem'] .= "\n" . urldecode(trim($strLinha));
                        } else {
                            $arrRegistro['mensagem'] = urldecode(trim($strLinha));
                        }
                    }
                    if (preg_match('/^Message:(.*)$/', $strLinha, $arrMatches)) {
//                        $arrRegistro['mensagem'] = trim($arrMatches[1]);
                        if (preg_match('/\[file "([^]]*)"\]/', $arrMatches[1], $arrMsgMatches)) {
                            $arrRegistro['file'] = $arrMsgMatches[1];
                        }
                        if (preg_match('/\[id "([0-9]*)"\]/', $arrMatches[1], $arrMsgMatches)) {
                            $arrRegistro['id'] = $arrMsgMatches[1];
                        }
                        if (preg_match('/\[data ([^]]*)"\]/', $arrMatches[1], $arrMsgMatches)) {
                            $arrRegistro['dados'] = trim($arrMsgMatches[1]);
                            if (preg_match('/ARGS:(.*): /', $arrMsgMatches[1], $arrMsg2Matches)) {
                                $arrRegistro['args'] = trim($arrMsg2Matches[1]);
                            }
                        }
                    }
                    break;
                case 'Z':
                    $strBloco = null;
                    break;
            }
        }
    }
    
    if (!empty($arrFiltros)) {
        foreach ($arrLog as $key => $val) {
            foreach ($arrFiltros as $keyFiltro => $valFiltro) {
                switch ($keyFiltro) {
                    case '403':
                        if (!preg_match('/^403/', $val['error'])) {
                            unset($arrLog[$key]);
                            continue 2;
                        }
                        break;
                    case 'getpost':
                        if (!preg_match('/GET|POST/', $val['metodo'])) {
                            unset($arrLog[$key]);
                            continue 2;
                        }
                        break;
                    case 'ignoreapache':
                        if (preg_match('/Apache-Error/', $val['mensagem'])) {
                            unset($arrLog[$key]);
                            continue 2;
                        }
                        break;
                }
            }
        }
    }
    return array_values($arrLog);
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Visualizador ModSecurity Audit</title>
        <meta name="author" content="Douglas Gomes de Souza - www.douglasdesouza.com.br">
        <link rel="stylesheet" type="text/css" href="css/auditor.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="css/dataTables.fixedHeader.min.css">
        <script type="text/javascript" language="javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/dataTables.fixedHeader.min.js"></script>
        <script type="text/javascript" language="javascript">
            var table;
            var erromsg = '<?php echo $strErrorMsg ?>';
            var dataSet = <?php echo json_encode($arrLog) ?>;
            $(document).ready(function () {

                var calcDataTableHeight = function () {
                    return Math.round($(window).height() * 0.63);
                };

                table = $('#tabela').DataTable({
                    "data": dataSet,
                    "columns": [
                        {
                            "className": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {"data": "ident", "title": "Ident"},
                        {"data": "data", "title": "Data/Hora"},
                        {"data": "host", "title": "Host"},
                        {"data": "ip_remoto", "title": "IP Remoto"},
                        {"data": "protocolo", "title": "Protocolo"},
                        {"data": "error", "title": "Erro"},
                        {"data": "metodo", "title": "Método"},
                        {"data": "id", "title": "RuleID"},
                        {"data": "uri", "title": "URI"}
                    ],
                    "order": [[2, "desc"]],
                    "pageLength": 100,
                    "scrollY": calcDataTableHeight(),
                    "scrollX": true,
                    "language": {
                        "lengthMenu": "Exibir _MENU_ registros por página",
                        "zeroRecords": "Nenhum registro encontrado.",
                        "info": "Exibindo página _PAGE_ de _PAGES_",
                        "infoEmpty": "Nenhum registro encontrado",
                        "infoFiltered": "(filtrados de um total de _MAX_ registros)",
                        "search": "Filtrar:",
                        "paginate": {
                            "first": "Primeira",
                            "last": "Última",
                            "next": "Próxima",
                            "previous": "Anterior"
                        },
                    }
                });
                //new $.fn.dataTable.FixedHeader( table );

            $(window).resize(function() {
                $('.dataTables_scrollBody').css('height', calcDataTableHeight());
                table.draw(true);
            });

            $('#tabela tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });

                // Add event listener for opening and closing details
                $('#tabela tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                    }
                });


                if (erromsg != '') {
                    alert(erromsg);
                }
            });

            function format(d) {
                return '<table class="detail">' +
                        '<tr><td>URI:</td><td>' + d.uri + '</td></tr>' +
                        '<tr><td>ARGS:</td><td>' + d.args + '</td></tr>' +
                        '<tr><td>POST:</td><td>' + d.post + '</td></tr>' +
                        '<tr><td>Referência:</td><td>' + d.referer + '</td></tr>' +
                        '<tr><td>Origin:</td><td>' + d.origin + '</td></tr>' +
                        '<tr><td>User-Agent:</td><td>' + d.user_agent + '</td></tr>' +
                        '<tr><td>Arquivo ModSec:</td><td>' + d.file + '</td></tr>' +
                        '<tr><td>Dados:</td><td><textarea>' + d.dados + '</textarea></td></tr>' +
                        '<tr><td>Mensagem Completa:</td><td><textarea>' + d.mensagem + '</textarea></td></tr>' +
                        '</table>';
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'] ?>"  method="POST">
                    <input type="hidden" name="acao" value="upload" />
                    Upload do Arquivo modsec_audit.log: <input type="file" name="arquivo" />
                    <input type="submit" value="Enviar"/>
                    <?php
                    $strMax = @ini_get('upload_max_filesize');
                    if ($strMax) {
                        echo "<br>Tamanho máximo permitido: $strMax";
                    }
                    ?>
                    <span style="float:right;">
                        Arquivo Carregado: 
                        <?php
                        if (isset($_SESSION['auditor_filename'])) {
                            echo $_SESSION['auditor_filename'];
                        } else {
                            echo 'Nenhum';
                        }
                        ?>
                    </span>
                    <fieldset>
                        <legend>Filtros:</legend>
                        <input type="checkbox" name="arrFiltros[403]" <?php echo isset($arrFiltros['403']) ? 'checked="checked"' : ''?>/>Somente Erros 403&nbsp;&nbsp;
                        <input type="checkbox" name="arrFiltros[getpost]" <?php echo isset($arrFiltros['getpost']) ? 'checked="checked"' : ''?>/>Somente GET e POST
                        <input type="checkbox" name="arrFiltros[ignoreapache]" <?php echo isset($arrFiltros['ignoreapache']) ? 'checked="checked"' : ''?>/>Ignorar erros Apache
                        <input type="button" value="Atualizar" onclick="document.forms[0].acao.value='';document.forms[0].submit()"/>
                    </fieldset>
                </form>
            </div>
            <table id="tabela" class="display cell-border compact">
            </table>
        </div>
    </body>
</html>